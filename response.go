package s3uploader

import (
	"encoding/json"
)

type ImagePath struct {
	ImagePath interface{} `json:"path"`
}

func CreateImagePath(path string) []uint8 {
	data := ImagePath{}
	data.ImagePath = path
	js, _ := json.Marshal(&data)
	return js
}
