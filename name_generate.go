package s3uploader

import (
	"crypto/rand"
	"encoding/base64"
	"io"
	"path"
)

func makeBase64Str() string {
	c := 15
	b := make([]byte, c)
	n, err := io.ReadFull(rand.Reader, b)
	if n != len(b) || err != nil {
		panic(err)
	}

	return base64.URLEncoding.EncodeToString(b)
}

func generate_image_name(route string) string {
	// Rename filename section
	newName := makeBase64Str()
	firstDir := string(newName[:3])
	secondDir := string(newName[3:6])
	fName := string(newName[6:])
	dirPath := path.Join(route + "/" + firstDir + "/" + secondDir + "/" + fName)

	return dirPath
}
