package s3uploader

import (
	"bytes"
	"fmt"
	"image"
	_ "image/jpeg"
	"image/png"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/disintegration/imaging"
	"github.com/mitchellh/goamz/s3"
	"github.com/oliamb/cutter"
)

func image_resize(original_image image.Image, width int, height int) []byte {
	m := imaging.Resize(original_image, width, height, imaging.Lanczos)
	imaging.Sharpen(m, 0.5)
	buf := new(bytes.Buffer)

	// z.B. for png: png.Encode(buf, new_image)
	// encode image to jpeg format
	//jpeg.Encode(buf, new_image, nil)
	//png.Encode(buf, m)
	png.Encode(buf, m)
	send_s3 := buf.Bytes()

	return send_s3
}

func ImageToByte(image image.Image) []byte {
	buf := new(bytes.Buffer)
	png.Encode(buf, image)
	image_bytes := buf.Bytes()
	return image_bytes
}

func GetCropPoints(p map[string][]string) []int {
	var points []int
	for _, value := range p {
		for _, y := range value {
			strtoint, _ := strconv.Atoi(y)
			points = append(points, strtoint)
		}
	}
	var croppoints []int
	croppoints = append(croppoints, points[0], points[1], points[2]-points[0], points[3]-points[1])

	return croppoints

}

func Resizer(route_sizes [][]interface{}, route string, req *http.Request, w http.ResponseWriter) {
	req.ParseMultipartForm(100000)
	raw_points := req.MultipartForm.Value
	fmt.Println(raw_points)
	points := GetCropPoints(raw_points)

	file, _, _ := req.FormFile("filename")
	data, err := ioutil.ReadAll(file)

	raw_image, _, err := image.Decode(bytes.NewReader(data))
	if err != nil {
		fmt.Println(err)
	}

	raw_byte_image, err := cutter.Crop(raw_image, cutter.Config{
		Width:  points[2],
		Height: points[3],
		Anchor: image.Point{points[0], points[1]},
		Mode:   cutter.TopLeft, // optional, default value
	})

	image_name := generate_image_name(route)
	AWS_s3 := s3_auth()

	// resize
	go func() {
		for item := range route_sizes {
			width64 := route_sizes[item][0].(int64)
			width := int(width64)
			height64 := route_sizes[item][1].(int64)
			height := int(height64)
			name := image_name + "_" + strconv.Itoa(height)
			resized_image := image_resize(raw_byte_image, width, height)

			AWS_s3.Put(name, resized_image, "", s3.PublicRead)
		}
		//crop_name := image_name + "_crop"
		orig_image := ImageToByte(raw_byte_image)
		AWS_s3.Put(image_name, orig_image, "", s3.PublicRead)
	}()
	answer := CreateImagePath(image_name)
	w.Header().Set("Content-Type", "application/json")
	w.Write(answer)
}
