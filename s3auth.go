package s3uploader

import (
	"github.com/joho/godotenv"
	"github.com/mitchellh/goamz/aws"
	"github.com/mitchellh/goamz/s3"
	"log"
	"os"
)

func s3_auth() *s3.Bucket {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	S3Bucket := os.Getenv("S3_BUCKET")
	S3AccessKey := os.Getenv("S3_ACCESS_KEY")
	S3SecretKey := os.Getenv("S3_SECRET_KEY")
	auth := aws.Auth{
		AccessKey: S3AccessKey,
		SecretKey: S3SecretKey,
	}
	region := aws.EUWest
	conn := s3.New(auth, region)
	bucket := conn.Bucket(S3Bucket)

	return bucket
}
